import { enqueueSnackbar } from 'notistack';
import { NavLink, Outlet } from 'react-router-dom';

export const App = () => {
  return (
    <>
      <button onClick={() => enqueueSnackbar('I love hooks')}>
        Show snackbar
      </button>
      <ul>
        <li>
          <NavLink to="/">Dashboard</NavLink>
        </li>
        <li>
          <NavLink to="/login">login</NavLink>
        </li>
        <li>
          <NavLink to="/signup">register</NavLink>
        </li>
      </ul>
      <hr />
      <Outlet />
    </>
  );
};
