import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
//
import { InputField } from '@ui/forms/InputField';
import { SubmitBtn } from '@ui/forms/SubmitBtn';
import { Container } from '@ui/htmlElements/Container';
import { fontPalette } from '@styles/colorPalette';

/****** styles ******/
const FormHeader = styled.div`
  font-family: 'Metropolis Regular', sans-serif;
  color: ${fontPalette.default};
  font-size: clamp(24px, 8vw + 4px, 48px);
  font-weight: 500;
  line-height: clamp(26px, 8vw + 4px, 56px);
  text-align: center;
`;

const SubFormHeader = styled.div`
  font-family: 'Metropolis Regular', sans-serif;
  color: ${fontPalette.grey};
  font-size: clamp(10px, 3vw, 15px);
  font-weight: 500;
  line-height: 24px;
  text-align: center;
  margin-bottom: clamp(22px, 5vw, 48px);
`;

const LoginLink = styled.div`
  display: flex;
  justify-content: center;
  font-family: 'Metropolis Regular', sans-serif;
  color: ${fontPalette.default};
  font-size: 15px;
  line-height: 24px;
`;

const StyledNavLink = styled(NavLink)`
  color: #78b4ed;
  font-weight: 500;
  text-decoration: none;
  transition: color 0.3s ease;

  &:hover {
    color: #548bbe;
  }
`;
/****** styles ******/

// for check email
const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;

// structure data for form
const SignupSchema = Yup.object().shape({
  Name: Yup.string()
    .min(2, 'must be more than 2 characters')
    .max(10, 'must be no more than 10 characters')
    .required('required field'),
  Email: Yup.string()
    .matches(emailRegex, 'invalid email address')
    .required('required field'),
  Password: Yup.string()
    .min(2, 'must be more than 2 characters')
    .required('required field'),
});

export const FormRegistration = () => {
  // initial value for form
  const initialValues = {
    Name: 'Dima',
    Email: 'a@aa.com',
    Password: '123',
  };

  // send form
  const handleSubmit = values => {
    console.log('sign:', values);
  };

  return (
    <Container>
      <FormHeader>Welcome</FormHeader>
      <SubFormHeader>Welcome! Please enter your details</SubFormHeader>
      <Formik
        initialValues={initialValues}
        validationSchema={SignupSchema}
        onSubmit={handleSubmit}
        autoComplete="off"
      >
        {({ errors, touched, values, handleChange }) => (
          <Form
            autoComplete="off"
            style={{
              display: 'flex',
              flexDirection: 'column',
              position: 'relative',
              gap: '16px',
              width: 'clamp(200px, 80vw, 500px)',
              maxWidth: '500px',
            }}
          >
            {/* Name */}
            <InputField
              name="Name"
              type="text"
              placeholder="Name"
              error={errors.Name}
              touched={touched.Name}
              values={values.Name}
              handleChange={handleChange}
            />
            {/* Email */}
            <InputField
              name="Email"
              type="email"
              placeholder="Email"
              error={errors.Email}
              touched={touched.Email}
              values={values.Email}
              handleChange={handleChange}
            />
            {/* Password */}
            <InputField
              name="Password"
              type="password"
              placeholder="Password"
              error={errors.Password}
              touched={touched.Password}
              values={values.Password}
              handleChange={handleChange}
            />
            <SubmitBtn type="submit">Create account</SubmitBtn>
          </Form>
        )}
      </Formik>
      <LoginLink>
        Or&nbsp;&nbsp;
        <StyledNavLink to="/login">Log in</StyledNavLink>
      </LoginLink>
    </Container>
  );
};
