export const backgroundPalette = {
  default: '#111216',
  grey: '#242731',
  purple: '#8941FF',
  purpleHover: '#6d39c2',
};

export const borderPalette = {
  default: '#3D4250',
};

export const fontPalette = {
  default: '#fff',
  grey: '#7D7F8E',
  warning: '#ff0000ab',
};
