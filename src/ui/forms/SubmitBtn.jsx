import styled from 'styled-components';
//
import { backgroundPalette, fontPalette } from '@styles/colorPalette';

const Button = styled.button`
  padding: 10px 16px;
  margin: clamp(22px, 5vw + 4px, 48px) 0;
  border-radius: 8px;
  height: clamp(48px, 12vw + 4px, 64px);
  width: 100%;
  cursor: pointer;
  transition: background-color 0.3s ease;
  border: 1px solid transparent;
  background-color: ${backgroundPalette.purple};

  font-family: 'Metropolis Regular', sans-serif;
  color: ${fontPalette.default};
  font-size: 15px;
  font-weight: 500;
  line-height: 24px;

  &:hover {
    background-color: ${backgroundPalette.purpleHover};
  }
`;

export const SubmitBtn = ({ type, children }) => {
  return <Button type={type}>{children}</Button>;
};
