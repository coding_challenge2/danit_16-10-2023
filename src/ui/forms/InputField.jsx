import styled from 'styled-components';
//
import {
  backgroundPalette,
  borderPalette,
  fontPalette,
} from '@styles/colorPalette';

const MyInput = styled.input`
  padding: 10px 16px;
  border-radius: 8px;
  height: clamp(48px, 9vw, 64px);
  width: 100%;
  border: 1px solid ${borderPalette.default};
  background-color: transparent;

  font-family: 'Metropolis Regular', sans-serif;
  color: ${fontPalette.grey};
  font-size: 15px;
  line-height: 24px;
`;

const ErrorMessage = styled.div`
  color: ${fontPalette.warning};
  position: absolute;
  font-size: 12px;
  margin-left: 40px;
  bottom: -9px;
  padding: 4px 9px 3px;
  border: 1px solid ${borderPalette.default};
  background: ${backgroundPalette.grey};
  border-radius: 5px;
`;

const InputWrapper = styled.div`
  padding: 0;
  margin: 0;
  position: relative;
`;

export const InputField = ({
  name,
  type,
  placeholder,
  error,
  touched,
  values,
  handleChange,
}) => {
  return (
    <InputWrapper>
      <MyInput
        type={type}
        name={name}
        placeholder={placeholder}
        onChange={handleChange}
        value={values}
      />
      {error && touched ? <ErrorMessage>{error}</ErrorMessage> : null}
    </InputWrapper>
  );
};
