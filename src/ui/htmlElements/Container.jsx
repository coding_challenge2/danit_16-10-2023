import styled from 'styled-components';

const MyContainer = styled.div`
  width: 100%;
  padding: 0 10px;
  margin: 0 auto;
`;

export const Container = ({ children }) => {
  return <MyContainer className="container">{children}</MyContainer>;
};
