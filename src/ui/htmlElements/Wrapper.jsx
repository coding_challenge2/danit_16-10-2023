import styled, { css } from 'styled-components';

import { backgroundPalette } from '@styles/colorPalette';

const MyWrapper = styled.div`
  margin: 0 auto;
  padding: 0 20px;
  position: relative;
  display: grid;
  grid-template-rows: 1fr;

  // content position
  ${(props) =>
    props.$center &&
    css`
      justify-content: center;
      align-items: center;
    `}

  // background color
  ${(props) =>
    props.$bg &&
    css`
      background-color: ${backgroundPalette[props.$bg] || '#111216'};
    `}
`;

export const Wrapper = ({ children, center, bg }) => {
  return (
    <MyWrapper $center={center} $bg={bg} className="wrapper">
      {children}
    </MyWrapper>
  );
};
