import axios from 'axios';

export const axiosInstance = axios.create({
  timeout: 5000,
  baseURL: 'http://vindev.cx.ua',
});

/*

// Настройка интерсепторов при создании axiosInstance.
axiosInstance.interceptors.request.use(
  (config) => {
    // Добавьте логику перед отправкой запроса.
    return config;
  },
  (error) => {
    // Обработка ошибок запроса.
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    // Обработка успешного ответа.
    return response;
  },
  (error) => {
    // Обработка ошибок ответа.
    return Promise.reject(error);
  }
);

*/
