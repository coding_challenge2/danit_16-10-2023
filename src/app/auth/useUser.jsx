import { useQuery } from 'react-query';
import { axiosInstance } from '../axios/axiosInstance';

async function getUser(user) {
  if (!user) return null;

  try {
    const response = await axiosInstance.get('/user', {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    });

    // delete
    console.log('getUser:', response);

    return response.data;
    //
  } catch (error) {
    console.error('Error during getUser:', error);
    return null;
    // throw error;
  }
}

export const useUser = () => {
  // get data from server (react-query)
  const {
    data: user,
    // isLoading,
    // isSuccess,
    // isError,
  } = useQuery({
    queryFn: () => getUser(user),
    // queryKey: ['coins', page],

    onError: () => {
      console.log('error load data');
    },

    staleTime: 1000 * 5,
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
  });

  return { user: user ?? null };
};
