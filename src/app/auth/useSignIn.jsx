import { useSnackbar } from 'notistack';
import { useMutation } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { axiosInstance } from '../axios/axiosInstance';

async function signIn({ email, password }) {
  try {
    const response = await axiosInstance.post('/signin', {
      email,
      password,
    });

    // delete
    console.log('SignIn:', response);

    return response;
    //
  } catch (error) {
    console.error('Error during signIn:', error);
    // return null;
    throw error;
  }
}

export const useSignIn = () => {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const { mutate: singInMutation } = useMutation({
    mutationFn: ({ email, password }) => signIn(email, password),
    onSuccess: data => {
      console.log('useSignIn:', data);
      navigate('/');
    },
    onError: error => {
      enqueueSnackbar(`Error on Sing in - ${error}`, {
        variant: 'error',
      });
    },
  });

  return singInMutation;
};
