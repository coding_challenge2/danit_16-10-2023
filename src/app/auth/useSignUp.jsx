import { useNavigate } from 'react-router-dom';
import { useMutation } from 'react-query';
import { useSnackbar } from 'notistack';
import { axiosInstance } from '../axios/axiosInstance';

async function signUp({ name, email, password }) {
  try {
    const response = await axiosInstance.post('/signup', {
      name,
      email,
      password,
    });

    // delete
    console.log('SignUp:', response);

    return response;
    //
  } catch (error) {
    console.error('Error during signUp:', error);
    // return null;
    throw error;
  }
}

export const useSignUp = () => {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const { mutate: singUpMutation } = useMutation({
    mutationFn: ({ name, email, password }) => signUp(name, email, password),
    onSuccess: data => {
      console.log('useSignUp:', data);
      navigate('/');
    },
    onError: error => {
      enqueueSnackbar(`Error on Sing up - ${error}`, {
        variant: 'error',
      });
    },
  });

  return singUpMutation;
};
