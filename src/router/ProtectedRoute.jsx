import { Navigate } from 'react-router-dom';

import { useUser } from '@app/auth/useUser';

export const ProtectedRoute = ({ children }) => {
  const { user } = useUser();

  if (!user) return <Navigate to="/login" replace />;
  return <>{children}</>;
};
