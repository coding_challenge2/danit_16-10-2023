import { createBrowserRouter } from 'react-router-dom';
//
import { App } from '@components/App';
import { DashboardPage } from '@pages/DashboardPage';
import { LoginInPage } from '@pages/LoginInPage';
import { SignUpPage } from '@pages/SignUpPage';
import { NotFoundPage } from '@pages/NotFoundPage';
import { ProtectedRoute } from './ProtectedRoute';

//

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <NotFoundPage />,
    children: [
      {
        element: (
          <ProtectedRoute>
            <DashboardPage />
          </ProtectedRoute>
        ),
        index: true,
      },
      {
        path: 'login',
        element: <LoginInPage />,
      },
      {
        path: 'signup',
        element: <SignUpPage />,
      },
    ],
  },
]);
