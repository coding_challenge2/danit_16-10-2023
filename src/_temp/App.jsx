import axios from 'axios';
import { useState } from 'react';
import { useMutation, useQuery, useQueryClient } from 'react-query';

// fetch data form server
async function fetchData(skip) {
  const { data } = await axios.get(
    `https://api.coinstats.app/public/v1/coins?skip=${skip}&limit=10`
  );

  return data.coins;
}

// create new user
async function createUser(data) {
  const response = await axios.post('http://vindev.cx.ua/signup', data);
  console.log(response);
  return response;
}

export const App = () => {
  // state page for pagination
  const [page, setPage] = useState(0);

  // get data from server (react-query)
  const { data, isLoading, isSuccess, isError } = useQuery({
    queryFn: () => fetchData(page),
    queryKey: ['coins', page],
    onError: () => {
      console.log('error load data');
    },
    keepPreviousData: true,
    staleTime: 1000 * 5,
  });

  /*******************/

  const client = useQueryClient();

  const { mutate } = useMutation({
    mutationFn: (newUser) => createUser(newUser),
    // onSuccess: () => {
    //   // use after success response
    //   client.invalidateQueries({ queryKey: ['coins', page] });
    // },

    onSuccess: (response) => {
      console.log('mutate-response', response);
    },
  });

  // submit send signup form
  const onSubmit = (event) => {
    event.preventDefault();

    // get inputs data from form
    const formData = new FormData(event.target);
    const fields = Object.fromEntries(formData);

    // console.log(fields);
    // send data
    mutate(fields);

    // clear inputs
    event.target.reset();
  };

  /**********************/

  if (isLoading) {
    return <h3>Loading ...</h3>;
  }
  if (isError) {
    return <h3>Error receiving data from server...</h3>;
  }
  if (!data) {
    return <h3>Data is empty ...</h3>;
  }

  return (
    <>
      {data && (
        <>
          <div>
            {isSuccess && <p>Data is success</p>}
            <br />

            {data.map((item) => (
              <p key={item.id}>{`${item.name} - ${item.price}`}</p>
            ))}
          </div>
          <br />
          <button onClick={() => setPage((p) => p - 10)}>back</button>
          <button onClick={() => setPage((p) => p + 10)}>next</button>
        </>
      )}
      <br />
      <br />
      <hr />
      <br />
      <h2>Form login</h2>
      <br />
      <form onSubmit={onSubmit}>
        <input name="name" type="text" placeholder="name" />
        <input name="email" type="text" placeholder="email" />
        <input name="password" type="text" placeholder="password" />
        <button type="submit">send</button>
      </form>
    </>
  );
};
