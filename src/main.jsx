import ReactDOM from 'react-dom/client';
import { QueryClient, QueryClientProvider } from 'react-query';
import { RouterProvider } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
//
import { router } from './router/router';
//
import '@styles/index.css';

// create react-query client
const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById('root')).render(
  <QueryClientProvider client={queryClient}>
    <SnackbarProvider
      maxSnack={3}
      autoHideDuration={4000}
      style={{ borderRadius: '8px' }}
    />
    <RouterProvider router={router} />
  </QueryClientProvider>,
);
