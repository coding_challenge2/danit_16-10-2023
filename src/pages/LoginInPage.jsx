import { Wrapper } from '@ui/htmlElements/Wrapper';
import { FormLogin } from '@components/forms/FormLogin';

export const LoginInPage = () => {
  return (
    <Wrapper center={true} bg={'grey'}>
      <FormLogin />
    </Wrapper>
  );
};
