import { Wrapper } from '@ui/htmlElements/Wrapper';
import { FormRegistration } from '@components/forms/FormRegistration';

export const SignUpPage = () => {
  return (
    <Wrapper center={true} bg={'grey'}>
      <FormRegistration />
    </Wrapper>
  );
};
