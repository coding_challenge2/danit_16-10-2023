#Макет: https://www.figma.com/file/MZTpG2TlbQbCBf9OCAG66L/%D0%A2%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D0%B5-%D0%B7%D0%B0%D0%B2%D0%B4%D0%B0%D0%BD%D0%BD%D1%8F?node-id=0%3A1&t=KI90zD2fUOCMA0Iy-0

#Бекенд postman:
https://elements.getpostman.com/redirect?entityId=24753240-3e490737-dabd-450d-af5a-d7c3747172ca&entityType=collection

#Задача:
Створити скріни логіна, реєстрації, дашборда та підключити їх до бекенда
Доступ до дашборду має бути тільки для авторизованого юзера

### ------------------------------------------------

#Вимого:
● Стилізація за допомогою бібліотеки styled-components
● Для асинхронного зберігання використовувати бібліотеку react-query
● Для форм використовувати Formik or React hook form
● Бібліотека для роутинга react router 6
● Компоненти, Блоки, та Скріни мають складатися з базових UI компонентів та не містити в собі нативних тегів HTML
● Імпотри з використанням алясів
● Структура компонентів має бути розбита на Screens, Blocks, Components, UI
● Не більше 120 рядків в файлі
● Для графіків можна використати бібліотеку recharts
● UI має бути піксель перфект

### ------------------------------------------------

Реалізація:

1. Vite
2. Axios
3. React-query
4. Formik + Yup
5. React-router-dom
6. Styled-components
7. Font Awesome - @fortawesome
8. notistack - Display notifications
